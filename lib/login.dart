import 'package:flutter/material.dart';
import 'package:flutter_application_1/SignUp.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Login"),
        ),
        body: ElevatedButton(
          onPressed: () => {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SignUp()))
          },
          child: const Text("SignUp"),
        ));
  }
}
