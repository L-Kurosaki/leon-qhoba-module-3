import 'package:flutter/material.dart';
import 'package:flutter_application_1/login.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign Up"),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.pop(
              context, MaterialPageRoute(builder: (context) => Login()))
        },
        child: const Text("Login"),
      ),
    );
  }
}
